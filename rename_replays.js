const { default: W3GReplay } = require("w3gjs");
const fs = require("fs");
const path = require('path');
const parser = new W3GReplay();

const originalReplayPath = ("/path/to/replays");    // replays to be renamed
const newReplayPath = ("/path/to/replays/renamed");    // where to save renamed replays


const parseAndRename = async (file) => {
    // get the last modified date of the file
    const stats = fs.statSync(file);
    const lastModified = new Date(stats.mtime);

    // format the date
    const month = lastModified.getMonth() + 1;
    const day = lastModified.getDate();
    const year = lastModified.getFullYear();
    const hour = lastModified.getHours();
    const minute = lastModified.getMinutes();
    const second = lastModified.getSeconds();
    const createdDate = `${month}_${day}_${year}_${hour}_${minute}_${second}`;

    // parse the replay
    const result = await parser.parse(file);
    var team_0 = [];
    var team_1 = [];
    for (let p = 0; p < result.players.length; p++) {
        if (result.players[p].teamid == 0) {
            team_0.push({playerName: result.players[p].name, playerRace: result.players[p].raceDetected})
        } else if (result.players[p].teamid == 1) {
            team_1.push({playerName: result.players[p].name, playerRace: result.players[p].raceDetected})
        }
    }

    // build team strings
    var team_0_string = "";
    var team_1_string = "";
    for (let i = 0; i < team_0.length; i++) {
        team_0_string += team_0[i].playerName + "[" + team_0[i].playerRace + "]";
        if (i < team_0.length - 1) {
            team_0_string += "+";
        }
    }

    for (let i = 0; i < team_1.length; i++) {
        team_1_string += team_1[i].playerName + "[" + team_1[i].playerRace + "]";
        if (i < team_1.length - 1) {
            team_1_string += "+";
        }
    }

    // build full string for new replay name
    var full_string = "[" + createdDate + "]" + team_0_string + "_VS_" + team_1_string;
    console.log(full_string)
    fs.rename(
        file,
        newReplayPath + full_string + ".w3g",
        function(err) {
        if (err) {
            console.log(err);
        } else {
            console.log("Replay renamed to " + full_string + ".w3g");
        }
    });
}

const getReplayFiles = (dir) => {
    return new Promise((resolve, reject) => {
        fs.readdir(dir, (err, files) => {
            if (err) reject(err);

            const replayFiles = files.filter(file => path.extname(file) === '.w3g');
            resolve(replayFiles);
        });
    });
}

// main function to parse and rename all replay files in a directory
const parseAndRenameAll = async (dir) => {
    try {
        const replayFiles = await getReplayFiles(dir);

        for (let i = 0; i < replayFiles.length; i++) {
            const file = path.join(dir, replayFiles[i]);
            await parseAndRename(file);
        }
    } catch (err) {
        console.error(`Error getting replay files: ${err}`);
    }
}

// call main function
parseAndRenameAll(originalReplayPath)
