# w3g-parser
This is a simple script that uses w3gjs to parse a w3g file and rename LastReplay.w3g.

## Sample output
### 1v1
`[12_19_2022_15_45_24]Neo#2529[U]_VS_carsoNNN#1657[O].w3g`

### 4v4
`[1_22_2023_12_24_45]Shocker#21685[U]+Grubby#1278[O]+Teddster#1274[O]+GodLikeSoul#2607[O]_VS_ToD#2792[H]+BiGToBi#21514[U]+FChampRaRa#1204[H]+edholm#21389[U].w3g`

## Requirements
* nodejs
* w3gjs

## Set up
Install w3gjs with the following command
`npm install`

Modify replay paths in `rename_replays.js`
```
const originalReplayPath = ("/path/to/replays");    // replays to be renamed
const newReplayPath = ("/path/to/replays/renamed");    // where to save renamed replays
```

Run the script
`node rename_replays.js`
